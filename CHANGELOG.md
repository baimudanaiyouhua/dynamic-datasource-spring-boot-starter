# v1.4.0

- 支持了在类上注解，如果方法上同时有注解则方法注解优先。
- 支持了遇到事物强制主库，并且是默认行为，可在配置更改foeceMaster。
- 最低支持jdk1.7，springboot1.4.x。
- 重构aop，解决了部分springboot版本引入插件无效的问题。

# v1.3.0

- 对Druid的paCache属性提供支持。
- 还原上一版本切面的配置方式。
- 其他一些细节的优化。

# v1.2.0

- 对Druid提供更完善的支持。
- 更改了默认的注解切面的注入方式。
- 抽象了切面选择数据源接口，方便以后支持spel语法等。

# v1.1.0

- 支持Druid数据源 (support DruidDataSource)。
